<?php

/**
 * @defgroup plugins_generic_siteUsage Plugin
 */

/**
 * @file plugins/generic/siteUsage/index.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_generic_siteUsage
 *
 */

require_once('SiteUsagePlugin.inc.php');
return new SiteUsagePlugin();
