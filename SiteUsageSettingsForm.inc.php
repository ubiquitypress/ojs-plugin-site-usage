<?php

/**
 * @file plugins/generic/siteUsage/SiteUsageSettingsForm.inc.php
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @class SiteUsageSettingsForm
 * @ingroup plugins_generic_siteUsage
 *
 * @brief Form for journal managers to modify announcement feed plugin settings
 */

import('lib.pkp.classes.form.Form');

class SiteUsageSettingsForm extends Form {

	/** @var int */
	protected $_journalId;

	/** @var object */
	protected $_plugin;

	/**
	 * Constructor
	 * @param $plugin object
	 * @param $journalId int
	 */
	public function __construct($plugin, $journalId) {
		$this->_journalId = $journalId;
		$this->_plugin = $plugin;

		parent::__construct($plugin->getTemplateResource('settingsForm.tpl'));
		$this->addCheck(new FormValidatorPost($this));
		$this->addCheck(new FormValidatorCSRF($this));
	}

	/**
	 * Initialize form data.
	 */
	public function initData() {
		$journalId = $this->_journalId;
		$plugin = $this->_plugin;

		$this->setData('embeddedCode', $plugin->getSetting($journalId, 'embeddedCode'));
	}

	/**
	 * Assign form data to user-submitted data.
	 */
	public function readInputData() {
		$this->readUserVars(['embeddedCode']);

		// check that embeddedCode value is set
		$data = $this->getData('embeddedCode');
		if($data !== '') $this->setData('embeddedCode', $data);
	}

	/**
	 * Fetch the form.
	 * @copydoc Form::fetch()
	 */
	public function fetch($request, $template = null, $display = false) {
		$templateMgr = TemplateManager::getManager($request);
		$templateMgr->assign('pluginName', $this->_plugin->getName());
		return parent::fetch($request, $template, $display);
	}

	/**
	 * @copydoc Form::execute()
	 */
	public function execute(...$functionArgs) {
		$plugin = $this->_plugin;
		$journalId = $this->_journalId;
		$plugin->updateSetting($journalId, 'embeddedCode', $this->getData('embeddedCode'));

		parent::execute(...$functionArgs);
	}

}
