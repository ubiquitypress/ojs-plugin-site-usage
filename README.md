# OJS Site Usage Plugin

# What's new
- A new dashboard page `Site usage` allowing the display of embedded code for site analytics
- A sidebar link to navigate to this page 'Statistics > **Site usage**'
- A plugin setting value that is the code to be embedded, for example the Plausible analytics embedded code
- This is only available to Journal Managers

# Installation

- Compress the plugin in a `tar.gz` file and upload it through OJS plugin manager.
- Please make sure that the plugin folder name is `siteUsage`

- Ability to add the data {$excludedReviewers} and {$recommendedReviewers} to mail templates

## OJS compatibility

* OJS `3.2.1-*`+: please install version `0.7.*`+
* OJS `3.3.0-14`+: please install version `0.7.10`+
