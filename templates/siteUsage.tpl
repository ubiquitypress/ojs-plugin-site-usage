{extends file="layouts/backend.tpl"}
{block name="page"}
	<div id="errorMessage" style="text-align: center; display: none">{translate key="plugins.generic.siteUsage.unableToLoadData"}</div>
	<div id="embeddedCode">{$embeddedCode}</div>
	<script defer>
		$(document).ready(function() {ldelim}
			let iframes = document.querySelectorAll('iframe');
			for (let i = 0; i < iframes.length; i++) {
				let iframe = iframes[i];
				if (iframe.hasAttribute('plausible-embed')) {
					let script = document.createElement('script');
					script.src = 'https://plausible.io/js/embed.host.js';
					script.async = true;
					script.onerror = function() {
						$("#errorMessage").css('display', 'block');
					};
					document.head.appendChild(script);
				}
			}
		});
	</script>
{/block}
