{**
 * plugins/generic/siteUsage/settingsForm.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * Site usage plugin settings
 *
 *}
<script>
	$(function() {ldelim}
		// Attach the form handler.
		$('#siteUsageSettingsForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
        {rdelim});
</script>

<form class="pkp_form" id="siteUsageSettingsForm" method="post" action="{url router=$smarty.const.ROUTE_COMPONENT op="manage" category="generic" plugin=$pluginName verb="settings" save=true}">
	<div id="siteUsageSettings">
        {csrf}
        {include file="common/formErrors.tpl"}
        {fbvFormSection title="plugins.generic.siteUsage.settings.embeddedCode" for="embeddedCode" required=true}
        {fbvElement type="textarea" name="embeddedCode" id="embeddedCode" value=$embeddedCode }
        {/fbvFormSection}
        {fbvFormButtons}
		<p><span class="formRequired">{translate key="common.requiredField"}</span></p>
	</div>
</form>
