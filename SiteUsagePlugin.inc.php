<?php

/**
 * @file plugins/generic/siteUsage/SiteUsagePlugin.inc.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SiteUsagePlugin
 * @ingroup plugins_generic_SiteUsagePlugin
 *
 * @brief SiteUsage plugin class
 */

import('lib.pkp.classes.plugins.GenericPlugin');
class SiteUsagePlugin extends GenericPlugin {
    /**
     * @copydoc Plugin::register()
     */
    function register($category, $path, $mainContextId = null) {
        $success = parent::register($category, $path, $mainContextId);
        if ($success && $this->getEnabled($mainContextId)) {
			HookRegistry::register('TemplateManager::display', array($this, 'handleTemplateDisplay'));
			HookRegistry::register('LoadHandler', [$this, 'setupHandler']);
		}
        return $success;
    }

    /****************/
    /**** Plugin ****/
    /****************/

    /**
    * @copydoc Plugin::isSitePlugin()
    */
    function isSitePlugin() {
        // This is a site-wide plugin.
        return true;
    }

    /**
     * @copydoc Plugin::getDisplayName()
     * Get the plugin name
     */
    function getDisplayName() {
        return __('plugins.generic.siteUsage.displayName');
    }

    /**
     * @copydoc Plugin::getDescription()
     * Get the description
     */
    function getDescription() {
        return __('plugins.generic.siteUsage.description');
    }

    /**
     * @copydoc Plugin::getInstallSitePluginSettingsFile()
     * get the plugin settings
     */
    function getInstallSitePluginSettingsFile() {
        return $this->getPluginPath() . '/settings.xml';
    }

	function handleTemplateDisplay($hookName, $params) {
		$request = PKPApplication::get()->getRequest();
		$context = $request->getContext();

		if ($context) {
			if ($this->isJournalManager()) {
				$templateMgr = &$params[0];
				$menu = $templateMgr->getState('menu');
				$router = $request->getRouter();

				$menu['statistics']['submenu']['site_usage'] = [
					'name' => __('plugins.generic.siteUsage.siteUsage'),
					'url' => $router->url($request, null, 'stats', 'siteUsage'),
					'isCurrent' => $request->getRequestedPage() === 'stats' && $request->getRequestedOp() === 'siteUsage'
				];

				$templateMgr->setState(['menu' => $menu]);
			}
		}
	}

	function setupHandler($hookName, $params) {
		$requestedPage = $params[0];
		$requestedOperation = $params[1];

		if ($this->isJournalManager() && $requestedPage == "stats" && $requestedOperation == "siteUsage") {
			$this->import('pages.SiteUsageHandler');
			define('HANDLER_CLASS', 'SiteUsageHandler');

			return true;
		}

		return false;
	}

	function isJournalManager(): bool
	{
		$request = PKPApplication::get()->getRequest();
		$user = $request->getUser();
		if(!$user) {
			return false;
		}
		$context = $request->getContext();
		if($context) {
			$contextId = $context->getId();
			$roleDao = DAORegistry::getDAO('RoleDAO'); /* @var $roleDao RoleDAO */
			return $roleDao->userHasRole($contextId, $user->getId(), ROLE_ID_MANAGER);
		}

		return false;
	}

	/**
	 * @copydoc Plugin::getActions()
	 */
	public function getActions($request, $verb) {
		$router = $request->getRouter();
		import('lib.pkp.classes.linkAction.request.AjaxModal');
		return array_merge(
			$this->getEnabled()?array(
				new LinkAction(
					'settings',
					new AjaxModal(
						$router->url($request, null, null, 'manage', null, array('verb' => 'settings', 'plugin' => $this->getName(), 'category' => 'generic')),
						$this->getDisplayName()
					),
					__('manager.plugins.settings'),
					null
				),
			):array(),
			parent::getActions($request, $verb)
		);
	}

	public function manage($args, $request) {
		switch ($request->getUserVar('verb')) {
			case 'settings':
				$context = $request->getContext();

				AppLocale::requireComponents(LOCALE_COMPONENT_APP_COMMON,  LOCALE_COMPONENT_PKP_MANAGER);
				$templateMgr = TemplateManager::getManager($request);
				$templateMgr->registerPlugin('function', 'plugin_url', array($this, 'smartyPluginUrl'));

				$this->import('SiteUsageSettingsForm');
				$form = new SiteUsageSettingsForm($this, $context->getId());

				if ($request->getUserVar('save')) {
					$form->readInputData();
					if ($form->validate()) {
						$form->execute();
						return new JSONMessage(true);
					}
				} else {
					$form->initData();
				}
				return new JSONMessage(true, $form->fetch($request));
		}
		return parent::manage($args, $request);
	}
}

