#
msgid ""
msgstr "Language: sr_RS@latin\n"

msgid "plugins.generic.siteUsage.displayName"
msgstr "Употреба сајта"

msgid "plugins.generic.siteUsage.description"
msgstr ""
"Компјутерска табла за употребу сајта са везом бочне траке за менаџере "
"часописа [одржава се убикуити пресс]."

msgid "plugins.generic.siteUsage.siteUsage"
msgstr "Употреба сајта"

msgid "plugins.generic.siteUsage.settings.embeddedCode"
msgstr "Уграђени кодекс контролне табле"

msgid "plugins.generic.siteUsage.unableToLoadData"
msgstr "Није могуће учитати податке.То може бити последица блокатора огласа"
