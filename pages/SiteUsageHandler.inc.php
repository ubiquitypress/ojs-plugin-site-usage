<?php
import('classes.handler.Handler');

class SiteUsageHandler extends Handler {

	public $_isBackendPage = true;

	private $settingValue;

	function __construct($settingValue) {
		$this->settingValue = $settingValue;
	}

	function siteUsage($args, $request) {
		$plugin = PluginRegistry::getPlugin('generic', 'siteusageplugin');
		$templateMgr = TemplateManager::getManager($request);
		$this->setupTemplate($request);
		$contextId = $request->getContext()->getId();
		$embeddedCode = $plugin->getSetting($contextId, 'embeddedCode');
		$templateMgr->assign('embeddedCode', $embeddedCode);

		return $templateMgr->display($plugin->getTemplateResource('siteUsage.tpl'));
	}

}
